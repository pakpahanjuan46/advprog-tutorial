package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class DepokedVeggies implements Veggies {

    public String toString() {
        return "Depok original vegetable";
    }
}
