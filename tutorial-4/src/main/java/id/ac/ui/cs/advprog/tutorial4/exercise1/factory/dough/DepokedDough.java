package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class DepokedDough implements Dough {
    public String toString() {
        return "Medium well done dough";
    }
}
