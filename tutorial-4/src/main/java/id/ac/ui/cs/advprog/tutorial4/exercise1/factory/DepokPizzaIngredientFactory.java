package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.DepokedCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.DepokedClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.DepokedDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.DepokedSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.DepokedVeggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new DepokedDough();
    }

    public Sauce createSauce() {
        return new DepokedSauce();
    }

    public Cheese createCheese() {
        return new DepokedCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlives(), new DepokedVeggies(), new Mushroom(), new Garlic()};
        return veggies;
    }

    public Clams createClam() {
        return new DepokedClams();
    }
}
