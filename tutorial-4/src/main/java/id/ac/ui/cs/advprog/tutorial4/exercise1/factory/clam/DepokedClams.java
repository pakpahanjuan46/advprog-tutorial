package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class DepokedClams implements Clams {

    public String toString() {
        return "Depoked Clams from Sun City Depok";
    }
}
