package id.ac.ui.cs.advprog.tutorial4.excercise1;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class FactoryTest {
    PizzaStore dpkPizzaStore;
    PizzaStore nyPizzaStore;

    @Before
    public void setUp() throws Exception {
        dpkPizzaStore = new DepokPizzaStore();
        nyPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testDepokPizzaStoreCanSellCheesePizza() {
        Pizza pizza = dpkPizzaStore.orderPizza("cheese");
        boolean check = pizza instanceof CheesePizza;
        assertTrue("Depok Pizza Store cannot sell cheese pizza", check);
    }

    @Test
    public void testDepokPizzaStoreCanSellClamPizza() {
        Pizza pizza = dpkPizzaStore.orderPizza("clam");
        boolean check = pizza instanceof ClamPizza;
        assertTrue("Depok Pizza Store cannot sell clam pizza", check);
    }

    @Test
    public void testDepokPizzaStoreCanSellVeggiePizza() {
        Pizza pizza = dpkPizzaStore.orderPizza("veggie");
        boolean check = pizza instanceof VeggiePizza;
        assertTrue("New York Pizza Store cannot sell veggie pizza", check);
    }

    @Test
    public void testNewYorkPizzaStoreCanSellCheesePizza() {
        Pizza pizza = nyPizzaStore.orderPizza("cheese");
        boolean check = pizza instanceof CheesePizza;
        assertTrue("New York Pizza Store cannot sell cheese pizza", check);
    }

    @Test
    public void testNewYorkPizzaStoreCanSellClamPizza() {
        Pizza pizza = nyPizzaStore.orderPizza("clam");
        boolean check = pizza instanceof ClamPizza;
        assertTrue("New York Pizza Store cannot sell clam pizza", check);
    }

    @Test
    public void testNewYorkPizzaStoreCanSellVeggiePizza() {
        Pizza pizza = nyPizzaStore.orderPizza("veggie");
        boolean check = pizza instanceof VeggiePizza;
        assertTrue("New York Pizza Store cannot sell veggie pizza", check);
    }
}
