package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyRocketPowered implements FlyBehavior {
    public void fly() {
        System.out.println("I'm dlying with a rocket!");
    }
}
